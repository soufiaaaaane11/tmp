let f = document.getElementById("input1")
let c = document.getElementById("input2")
let brd = document.getElementById("brd")

f.addEventListener('change', function(e){
    let a = e.target.value;
    if (isNaN(a)){
        brd.style.border = '3px dotted red'
    }
    else{
        a = Number((a - 32) * 5/9)
        c.setAttribute('value',a)
        brd.style.border = '3px dotted black'
    }
});
c.addEventListener('change', function(e){
    let b = e.target.value;
    if (isNaN(b)){
        brd.style.border = '3px dotted red'
    }
    else{
        b = Number( 32 + b * 9/5)
        f.setAttribute('value',b)
        brd.style.border = '3px dotted black'
    }
});
